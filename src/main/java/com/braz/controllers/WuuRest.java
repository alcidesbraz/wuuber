package com.braz.controllers;

import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.braz.dao.CorridaDaoImpl;
import com.braz.dao.MotoristaDaoImpl;
import com.braz.dao.PassageiroDaoImpl;
import com.braz.entities.Corrida;
import com.braz.entities.Motorista;
import com.braz.entities.Passageiro;

/*JAX-RS*/
@Path("/json")
public class WuuRest {

	private MotoristaDaoImpl motodao;
	private PassageiroDaoImpl passageiroDao;
	private CorridaDaoImpl corridaDao;
	private static Validator validator;

	public WuuRest() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@GET
	@Path(value = "/motorista")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Motorista> getMotorista() {
		motodao = new MotoristaDaoImpl();
		return motodao.motoristas();
	}

	@GET
	@Path(value = "/passageiro")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Passageiro> getPassageiro() {
		passageiroDao = new PassageiroDaoImpl();
		return passageiroDao.passageiros();
	}

	@GET
	@Path(value = "/corrida")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Corrida> getCorrida() {
		corridaDao = new CorridaDaoImpl();
		return corridaDao.corridas();
	}

	@POST
	@Path("/salvar")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Response salvarMotorista(Motorista motorista1) {

		Set<ConstraintViolation<Motorista>> constraintViolations = validator.validate(motorista1);

		if (constraintViolations.size() > 0) {
			return Response.ok().entity(constraintViolations.iterator().next().getMessage()).build();
		} else {
			motodao = new MotoristaDaoImpl();
			motodao.salvarMoto(motorista1);
			return Response.ok().entity("Salvo com sucesso!").build();
		}
	}

	@POST
	@Path("/salvar-passageiro")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Response salvarPassageiro(Passageiro passageiro) {

		Set<ConstraintViolation<Passageiro>> constraintViolations = validator.validate(passageiro);

		if (constraintViolations.size() > 0) {
			return Response.ok().entity(constraintViolations.iterator().next().getMessage()).build();
		} else {
			passageiroDao = new PassageiroDaoImpl();
			passageiroDao.salvarPassageiro(passageiro);
			return Response.ok().entity("Salvo com sucesso!").build();
		}
	}

	@POST
	@Path("/salvar-corrida")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Response salvarCorrida(Corrida corrida) {

		/*
		 * Há maneiras mais eficientes de de receber os objetos JSON atráves do
		 * mapeamento das tabelas através do OneToOne JoinColumn e Jackson
		 */

		Set<ConstraintViolation<Corrida>> constraintViolations = validator.validate(corrida);
		motodao = new MotoristaDaoImpl();

		int i = Integer.valueOf(corrida.getMotorista());

		boolean b = motodao.getMotoristaById(i);

		if (constraintViolations.size() > 0) {
			return Response.ok().entity(constraintViolations.iterator().next().getMessage()).build();
		} else {
			corridaDao = new CorridaDaoImpl();
			corridaDao.salvarCorrida(corrida);
			return Response.ok().entity("Salvo com sucesso!").build();
		}
	}
}
