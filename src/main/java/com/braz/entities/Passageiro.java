package com.braz.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

@Entity
@Table(name = "passageiro")
public class Passageiro implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String passageiroNome;
	
	@NotEmpty
	private String passageiroData;
	
	@NotEmpty
	private String passageiroSexo; 
	
	@NotEmpty
	@CPF(message="Por favor insira um CPF valido")
	private String passageiroCpf;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassageiroNome() {
		return passageiroNome;
	}

	public void setPassageiroNome(String passageiroNome) {
		this.passageiroNome = passageiroNome;
	}

	public String getPassageiroData() {
		return passageiroData;
	}

	public void setPassageiroData(String passageiroData) {
		this.passageiroData = passageiroData;
	}

	public String getPassageiroSexo() {
		return passageiroSexo;
	}

	public void setPassageiroSexo(String passageiroSexo) {
		this.passageiroSexo = passageiroSexo;
	}

	public String getPassageiroCpf() {
		return passageiroCpf;
	}

	public void setPassageiroCpf(String passageiroCpf) {
		this.passageiroCpf = passageiroCpf;
	}
}
