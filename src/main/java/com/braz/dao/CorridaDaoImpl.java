package com.braz.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.braz.entities.Corrida;
import com.braz.entities.Motorista;

public class CorridaDaoImpl implements CorridaDao {
	
	private EntityManager em;

	public CorridaDaoImpl() {
		em = new ConnectionFactory().getConnection();
	}

	@Override
	public void salvarCorrida(Corrida c) {
		try {
			em.getTransaction().begin();
			em.persist(c);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}		
	}

	@Override
	public List<Corrida> corridas() {
		Query query = em.createQuery("select c from Corrida c");
		List corridaLista = new ArrayList<Motorista>();
		corridaLista = query.getResultList();
		return corridaLista;
	}
}
