package com.braz.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.braz.entities.Motorista;

public class MotoristaDaoImpl implements MotoristaDao {

	private EntityManager em;

	public MotoristaDaoImpl() {
		em = new ConnectionFactory().getConnection();
	}

	@Override
	public void salvarMoto(Motorista m) {
		try {
			em.getTransaction().begin();
			em.persist(m);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

	@Override
	public List<Motorista> motoristas() {
		Query query = em.createQuery("select c from Motorista c");
		List motoLista = new ArrayList<Motorista>();
		motoLista = query.getResultList();
		return motoLista;
	}

	public boolean getMotoristaById(int id) {

		Query query = em.createQuery("select c.motoStatus from Motorista c where c.id = :id");
		query.setParameter("id", id);

		if (query.getSingleResult().equals("ativo")) {
			return true;
		} else {
			return false;
		}
	}
}
