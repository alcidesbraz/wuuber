package com.braz.dao;

import java.util.List;
import com.braz.entities.Motorista;

public interface MotoristaDao {
	public void salvarMoto(Motorista m);
	public List<Motorista> motoristas();
}
