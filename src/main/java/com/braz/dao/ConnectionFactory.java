package com.braz.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConnectionFactory {
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("wuuber");
	
	public EntityManager getConnection() {
		return emf.createEntityManager();
	}
}
